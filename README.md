# How to Run

## Deps

you need to create a settings.py file with all variables inside keys and so on

also you need to create all keys and secret access

https://developer.twitter.com/en/


```bash
python scrapy.py
```

settings.py example

```
TWITTER_KEY=''
TWITTER_SECRET=''
TWITTER_APP_KEY=''
TWITTER_APP_SECRET=''

```
