# == Import python libraries to deal with filetype and application (Twitter) python will deal with ==
import twitter
import csv
import settings

consumer_key=settings.TWITTER_KEY
consumer_secret=settings.TWITTER_SECRET
access_token=settings.TWITTER_APP_KEY
access_token_secret=settings.TWITTER_APP_SECRET


# == OAuth Authentication process ==
AUTH = twitter.oauth.OAuth(access_token, access_token_secret, consumer_key, consumer_secret)
TWITTER_API = twitter.Twitter(auth=AUTH)

# Create new csv file

csvfile = open('test.csv', 'w')
csvwriter = csv.writer(csvfile)

# Create csv headers corresponding to fields to be extracted from tweets
csvwriter.writerow(['created_at',
                    'user-screen_name',
                    'text',
                    'coordinates lng',
                    'coordinates lat',
                    'place',
                    'user-location',
                    'user-geo_enabled',
                    'user-lang',
                    'user-time_zone',
                    'user-statuses_count',
                    'user-followers_count',
                    'user-friends_count',
                    'user-created_at',
                    'user-source'])
# Define hashtag to look for
q = "test"
TRACK_TERMS = ['Argentina', 'Macri', 'Elecciones', 'macri', 'argentina']

# Print a text to show python is working

# print 'Trae post con el texto= "%s"' % (q)

# Access Twitter's streaming API
twitter_stream = twitter.TwitterStream(auth=TWITTER_API.auth)

# stream = twitter_stream.statuses.filter(track=q)

stream = twitter_stream.statuses.filter(track=q)


# Functions to clean and store data using dictionaries before writing it to the csv
def getVal(val):
    clean = ""
    if isinstance(val, bool):
        return val
    if isinstance(val, int):
        return val
    if val:
        clean = val.encode('utf-8')
    return clean

def getLng(val):
    if isinstance(val, dict):
        return val['coordinates'][0]

def getLat(val):
    if isinstance(val, dict):
        return val['coordinates'][1]

def getPlace(val):
    if isinstance(val, dict):
        return val['full_name'].encode('utf-8')


# main loop
for tweet in stream:
    try:                                                                # first part of error catching
        csvwriter.writerow([tweet['created_at'],                        # write data to csv file
                            getVal(tweet['user']['screen_name']),
                            getVal(tweet['text']),
                            getLng(tweet['coordinates']),
                            getLat(tweet['coordinates']),
                            getPlace(tweet['place']),
                            getVal(tweet['user']['location']),
                            getVal(tweet['user']['geo_enabled']),
                            getVal(tweet['user']['lang']),
                            getVal(tweet['user']['time_zone']),
                            getVal(tweet['user']['statuses_count']),
                            getVal(tweet['user']['followers_count']),
                            getVal(tweet['user']['friends_count']),
                            getVal(tweet['user']['created_at']),
                            getVal(tweet['source'])
                            ])
        csvfile.flush()
        print getVal(tweet['user']['screen_name']), getVal(tweet['text']), tweet['coordinates'], getPlace(tweet['place'])  # Show the user some tweet data so they know it's still working
    except Exception as e:                                              # Second part of error catching
        print e.message                                                 # Print error message
